﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageProcessor : MonoBehaviour {

	[SerializeField] Camera computeCameraLow = null;
	[SerializeField] Camera computeCameraHigh = null;

	[SerializeField] RenderTexture computeTexture = null;

	[SerializeField] Material computeMaterialLow = null;
	[SerializeField] Material computeMaterialHigh = null;

	[SerializeField] GameObject computePlaneLow = null;
	[SerializeField] GameObject computePlaneHigh = null;

	[SerializeField] Texture2D debugTexture; 

	void Start () {

		int scale = 4;
		int width = 640;
		int height = 480;

		float pos = (computeCameraLow.nearClipPlane + 0.01f);
		computeCameraLow.aspect = ((width)/(float)(height));
		computePlaneLow.transform.position = computeCameraLow.transform.position + computeCameraLow.transform.forward * pos;
		computePlaneLow.transform.localScale = new Vector3(scale,1,width/(float)height*scale);

		pos = (computeCameraHigh.nearClipPlane + 0.01f);
		computeCameraHigh.aspect = ((width)/(float)(height));
		computePlaneHigh.transform.position = computeCameraHigh.transform.position + computeCameraHigh.transform.forward * pos;
		computePlaneHigh.transform.localScale = new Vector3(scale,1,width/(float)height*scale);
			
		debugTexture = null;
		if (debugTexture != null)
		{
			YuvToRgb(debugTexture,false);
		}

	}

	public void YuvToRgb(Texture2D _tex, bool _low)
	{		
		if (_low) {
			computeMaterialLow.mainTexture = _tex;
			computeCameraLow.Render();
		}
		else
		{
			computeMaterialHigh.mainTexture = _tex;
			computeCameraHigh.Render();
		}




	}
}
