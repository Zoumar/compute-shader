﻿using UnityEngine;
using System.Collections;

public class cameraScript : MonoBehaviour {

	private AndroidJavaObject androidCameraActivity;
	Texture2D texLow = null;
	Texture2D texHigh = null;

	int lastPictureIndex = -10;
	byte[] lastPhoto;

	int width = 640;
	int height = 480;
	 
	public int exposure = 400;
	public int iso = 200;
	public int offset = 4;
	float timeAtStart = 0;
	int texturePointer = -1;
	int planeID = 0;
	byte[][] imageBufferLow; 
	byte[][] imageBufferHigh;
	int kernelID;

	bool copyingHigh = false;
	bool copyingLow = false;
	bool low = true;

	string debugString = "";

	Camera cameraComponent;

	[SerializeField] bool debug = true;

	[SerializeField] RenderTexture computeTextureLow = null;
	[SerializeField] RenderTexture computeTextureHigh = null;
	[SerializeField] ImageProcessor imageProcessor = null;

   public void Start()
   {

		timeAtStart = Time.time;	  	  	  		 	
		initCamera();

   }

	public void setIso(float _iso)
	{
		iso = (int)_iso;
		this.androidCameraActivity.Call("setIso",iso);
	}

	public void setOffset(float _off)
	{
		offset = (int)_off;
		this.androidCameraActivity.Call("setOffset",offset);
	}

	public void setExposure(float _exposure)
	{   		
		float _maxRange = 1000;
		float _normalization = Mathf.Sqrt(_maxRange)/_maxRange;
		_exposure = ((_maxRange- Mathf.Sqrt(_maxRange-_exposure)*_normalization)); //log scale

		printValue(_exposure.ToString());
		this.androidCameraActivity.Call("setExposure",(int)_exposure);
	}

   void initCamera()
   {			
		
		AndroidJNIHelper.debug = true;	 	  

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		this.androidCameraActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		texLow = new Texture2D(width,height,TextureFormat.RGB24,false);	
		texHigh = new Texture2D(width,height,TextureFormat.RGB24,false);	
		texturePointer = texLow.GetNativeTexturePtr().ToInt32();

		if (texturePointer != -1)
			this.androidCameraActivity.Call("setTexturePointer",texturePointer);

		lastPictureIndex = -8;

		this.androidCameraActivity.Call("initCamera");
		lastPictureIndex = -7;

   }

   void run(string _func)
   {
		this.androidCameraActivity.Call("runOnUiThread", new AndroidJavaRunnable(() => { this.androidCameraActivity.Call(_func); }));
   }

   void OnGUI()
   {		
		if (GUI.Button(new Rect(50,50,100,50),"ISO *2"))
		{			
			
			iso*=2;
			if (iso == 0)
			{
				iso = 1;
			}
			this.androidCameraActivity.Call("setIso",iso);

		}

		if (GUI.Button(new Rect(150,50,100,50),"ISO /2"))
		{			
			iso/=2;
			this.androidCameraActivity.Call("setIso",iso);
		}

		if (GUI.Button(new Rect(50,100,100,50),"EXP +"))
		{			
			exposure = exposure*2;
			this.androidCameraActivity.Call("setExposure",exposure);
		}

		if (GUI.Button(new Rect(150,100,100,50),"EXP -"))
		{			
			exposure = exposure/2;
			this.androidCameraActivity.Call("setExposure",exposure);
		}

		//GUI.DrawTexture (new Rect (0, 0, height, width), computeTextureLow);
		if (debug) {
			GUI.DrawTexture (new Rect (0, 0, height, width), texLow);
			GUI.DrawTexture (new Rect (0, width, height, width), texHigh);

			GUI.Label (new Rect (150, 50, 100, 50), "image buffer length: " + imageBufferLow.Length.ToString ());
			GUI.Label (new Rect (250, 50, 100, 50), "Shutter-speed: " + exposure.ToString ());
			GUI.Label(new Rect(10,140,100,50),"last picture index: "+lastPictureIndex.ToString());
			GUI.Label(new Rect(10,240,100,50), "fps: "+lastPictureIndex/(Time.time - timeAtStart));
			GUI.Label(new Rect(10,440,100,50), "Time: "+Time.time.ToString());
			GUI.Label(new Rect(10,740,100,50), "Debug: "+debugString);

		}
   }

    public void printValue(string _value) 
    {
    		debugString=_value;
    }



	public void loadTexture()
	{		
			
   		
   			if (planeID == 0)
			{
				int planeCount = this.androidCameraActivity.Call<int> ("getPlaneCount");
				imageBufferLow = new byte[planeCount][];
			}

			int _index = this.androidCameraActivity.Call<int> ("getTexturePointer");
			texturePointer = _index;

			lastPhoto = this.androidCameraActivity.Call<byte[]> ("getPictureData");
			imageBufferLow[planeID] = lastPhoto;
			planeID++;

	
	}

	IEnumerator sendTextureToGpu(bool _low)
	{
				
		if (_low) {
			copyingLow = true;
			int _pos, _posDiv;
			for (int x = 0; x < 640; x++) {
				for (int y = 0; y < 480; y++) {					
					_pos = (y * 640 + x)*3;
					_posDiv = (y / 4) * 640 + x / 2;

					//float _y = imageBufferLow [0] [_pos]   / 255.0f;
					//float _u = imageBufferLow [1] [_pos+1] / 255.0f;
					//float _v = imageBufferLow [2] [_pos+2] / 255.0f;

					texLow.SetPixel (639 - x, y, new Color (1, 0, 0));
				}

				if (x % 262 == 262) {
					yield return new WaitForEndOfFrame ();
				}

			}
			texLow.Apply ();

			copyingLow = false;
		}
		else
		{			
			copyingHigh = true;
			int _pos, _posDiv;
			for (int x = 0; x < 640; x++) {
				for (int y = 0; y < 480; y++) {					
					_pos = y * 640 + x;
					_posDiv = (y / 2) * 640;
					float _y = imageBufferHigh [0] [_pos];// / 255.0f;
					//float _u = imageBufferHigh [1] [_posDiv] / 255.0f;
					//float _v = imageBufferHigh [2] [_posDiv] / 255.0f;

					texHigh.SetPixel (639 - x, y, new Color (_y, _y, _y));
				}

				if (x % 262 == 262) {
					yield return new WaitForEndOfFrame ();
				}

			}
			texHigh.Apply ();
			//imageProcessor.YuvToRgb (texHigh, _low);
			copyingHigh = false;		
		}
	}


	public void finishTextureLow ()
	{		
		
		//low light

		//low = false;
		if (copyingLow == false) {			
			
			int _planeCount = this.androidCameraActivity.Call<int> ("getPlaneCount");

			if (imageBufferLow == null) {
				imageBufferLow = new byte[_planeCount][];
			}

			for (int _plane = 0; _plane < _planeCount; _plane++) {
				imageBufferLow [_plane] = this.androidCameraActivity.Call<byte[]> ("getPictureData", _plane, true);
			}

			texLow.LoadImage(imageBufferLow[0]);
			texLow.Apply();
			width = texLow.width;
			height = texLow.height;
			//imageProcessor.YuvToRgb (texLow, true);
			//StartCoroutine (sendTextureToGpu (true));		
		}
	
	}		//high light
	public void finishTextureHigh()
	{		
		{			
			//low = true;
			if (copyingHigh == false) {			
				
				int _planeCount = this.androidCameraActivity.Call<int> ("getPlaneCount");

				if (imageBufferHigh == null) {
					imageBufferHigh = new byte[_planeCount][];
				}

				for (int _plane = 0; _plane < _planeCount; _plane++) {
					imageBufferHigh [_plane] = this.androidCameraActivity.Call<byte[]> ("getPictureData", _plane, false);
				}

				texHigh.LoadImage(imageBufferLow[0]);
				texHigh.Apply();
				//imageProcessor.YuvToRgb (texHigh, false);
				//StartCoroutine (sendTextureToGpu (false));		
			}
		}
		
	}


	public void setHigh()
	{
		low = false;
	}

	public void setLow()
	{
		low = true;
	}
   

   public void nextPhoto()
   {
   		lastPictureIndex++;
   }

   public void nextImage()
   {
		lastPictureIndex++;
   }

}

