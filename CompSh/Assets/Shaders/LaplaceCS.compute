//////////////////////////////////////
//                                  //
//	IMPLEMENTED KERNELS             //
//                                  //
//////////////////////////////////////
#pragma kernel LaplaceCS

//////////////////////////////////////
//                                  //
//	USED STRUCTRURES                //
//                                  //
//////////////////////////////////////    
struct Pixel
{
	float3 color;
};

//////////////////////////////////////
//                                  //
//	PASSED DATA                     //
//                                  //
//////////////////////////////////////
Texture2D<float4> orig;
SamplerState samplerOrig;

RWStructuredBuffer<Pixel> data;

uint width;
uint height;
uint mipLevels;
uint mipSize;
uint frame;
uint frameCount;

RWTexture2D<float4> laplace;

//////////////////////////////////////
//                                  //
//	FUNCTIONS DECLARATIONS          //
//                                  //
//////////////////////////////////////
#include "HelperCS.compute"
float3 LaplaceB (uint2 id, uint mipLevel, uint frame);

//////////////////////////////////////
//                                  //
//	KERNELS                         //
//                                  //
//////////////////////////////////////
[numthreads(8,8,1)]
void LaplaceCS (uint3 id : SV_DispatchThreadID) {
	for (uint i = 0; i <= mipLevels; ++i) {
		if (id.x < width/pow2[i] && id.y < height/pow2[i]) {
    		data[Coord(id.xy, i, LAPLACE, frame)].color = LaplaceB(id.xy,i,frame);
    		laplace[id.xy] = float4(data.Load(Coord(id.xy, i, LAPLACE, frame)).color,1);
    	}
	}
}

//////////////////////////////////////
//									//
//	HELPER FUNCTIONS				//
//									//
//////////////////////////////////////
float3 LaplaceB (uint2 id, uint mipLevel, uint frame) {
	float3 color = float3(0,0,0);
	if (mipLevel == 0) {
		color = orig.Load(uint3(id, 0)).xyz - 
				SubsamplePixel(id/2, mipLevel, GAUSS, frame);
	} else if (mipLevel < mipLevels) {
		color = SubsamplePixel(id, mipLevel-1, GAUSS, frame) -
				SubsamplePixel(id/2, mipLevel, GAUSS, frame);
	} else {
		color = SubsamplePixel(id, mipLevel-1, GAUSS, frame);
	}
	return color;
}