//////////////////////////////////////
//                                  //
//	IMPLEMENTED KERNELS             //
//                                  //
//////////////////////////////////////
#pragma kernel CollapseCS

//////////////////////////////////////
//                                  //
//	USED STRUCTRURES                //
//                                  //
//////////////////////////////////////    
struct Pixel
{
	float3 color;
};

//////////////////////////////////////
//                                  //
//	PASSED DATA                     //
//                                  //
//////////////////////////////////////
RWStructuredBuffer<Pixel> data;

uint width;
uint height;
uint mipLevels;
uint mipSize;
uint frameCount;
uint phase;

RWTexture2D<float4> result;

//////////////////////////////////////
//                                  //
//	FUNCTIONS DECLARATIONS          //
//                                  //
//////////////////////////////////////
#include "HelperCS.compute"
float3 Collapse (uint2 id, uint mipLevel);

//////////////////////////////////////
//                                  //
//	KERNELS                         //
//                                  //
//////////////////////////////////////
[numthreads(8,8,1)]
void CollapseCS (uint3 id : SV_DispatchThreadID) {
	if (id.x < width/pow2[phase] && id.y < height/pow2[phase]) {
		//	GAUSS here is not semanticaly GAUSS - it only means first MIP pyramid
		data[Coord(id.xy, phase, GAUSS, frameCount)].color = Collapse(id.xy,phase);
		result[id.xy] = float4(data.Load(Coord(id.xy, phase, GAUSS, frameCount)).color,1);
	}
}

//////////////////////////////////////
//									//
//	HELPER FUNCTIONS				//
//									//
//////////////////////////////////////
float3 Collapse (uint2 id, uint mipLevel) {
	float3 color = float3(0,0,0);
	// GAUSS here is not semanticaly Blurred image - for resulting frame it is his (Laplacian) MIP pyramid
	color = data.Load(Coord(id/2, mipLevel+1, GAUSS, frameCount)).color +
			data.Load(Coord(id, mipLevel, GAUSS, frameCount)).color;
	return color;
}