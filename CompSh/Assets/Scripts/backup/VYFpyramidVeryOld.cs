﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VYFpyramid_backup : MonoBehaviour {

	public ComputeShader compSh;
	public GameObject[] planes;

	int width;
	int height;
	int kernelHandle;
	RenderTexture laplace0;
	RenderTexture laplaceMip;
	RenderTexture gauss0;
	RenderTexture gaussMip;
	Texture2D texOrig;
	Renderer [] rends;

	// Use this for initialization
	void Start () {
		rends = new Renderer[4];
		rends [0] = GetComponent<Renderer> ();
		for (int i = 0; i < 4; ++i) {
			rends [1] = planes [0].GetComponent<Renderer> ();
			rends [2] = planes [1].GetComponent<Renderer> ();
			rends [3] = planes [2].GetComponent<Renderer> ();
		}

		texOrig = (Texture2D) rends[0].material.GetTexture ("_MainTex");

		width = texOrig.width;
		height = texOrig.height;

		laplace0 = ReadyTexture ();
		laplaceMip = ReadyTexture ();
		gauss0 = ReadyTexture ();
		gaussMip = ReadyTexture ();

		kernelHandle = compSh.FindKernel ("CSMain");
		/*
		compSh.SetTexture (kernelHandle, "laplace0", Laplace0);
		compSh.SetTexture (kernelHandle, "laplaceMip", LaplaceMip);
		compSh.SetTexture (kernelHandle, "gauss0", Gauss0);
		compSh.SetTexture (kernelHandle, "gaussMip", GaussMip);
		compSh.SetTexture (kernelHandle, "orig", texOrig);
		*/
		compSh.SetTexture (kernelHandle, "laplace0", laplace0);
		compSh.SetTexture (kernelHandle, "laplaceMip", laplaceMip);
		compSh.SetTexture (kernelHandle, "gauss0", gauss0);
		compSh.SetTexture (kernelHandle, "gaussMip", gaussMip);

		compSh.SetTexture (kernelHandle, "orig", texOrig);

		compSh.SetInt ("width", width);
		compSh.SetInt ("height", height);

		compSh.Dispatch (kernelHandle, width/8, height/8, 1);

		rends [0].material.SetTexture ("_MainTex", laplace0);
		rends [1].material.SetTexture ("_MainTex", laplaceMip);
		rends [2].material.SetTexture ("_MainTex", gauss0);
		rends [3].material.SetTexture ("_MainTex", gaussMip);
	}
	
	// Update is called once per frame
	void Update () {

		//rend.material.SetTexture("_MainTex",texShader);
		//rend.material.SetTextureScale("_MainTex",new Vector2 (64f/dim,64f/dim));
		//rend.material.mainTexture = tex;
	}

	RenderTexture ReadyTexture() {
		//RenderTexture tex = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		RenderTexture tex = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB);
		tex.enableRandomWrite = true;
		tex.Create ();
		return tex;
	}
}
