﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class VYFpyramid170401a: MonoBehaviour {

	public ComputeShader GaussCS;
	public ComputeShader LaplaceCS;
	public ComputeShader WeightsCS;
	public ComputeShader CombineCS;
	public ComputeShader CollapseCS;
	public ComputeShader CoordCS;
	public GameObject[] planes;

	int width;
	int height;
	int kernelHandle;
	int phase;
	int groupSize;
	int MIPpixelCount;
	int MIPlevels;
	int frameCount;
	float contrastCoefficient;
	float exposednessCoefficient;
	//float exposednessSigma;
	RenderTexture RT01;
	RenderTexture RT02;
	RenderTexture RT03;
	RenderTexture RT04;
	Texture2D[] texOrig;
	Renderer [] rends;
	ComputeBuffer computeBuffer;

	// Use this for initialization
	void Start () {
		rends = new Renderer[4];
		rends [0] = GetComponent<Renderer> ();
		for (int i = 0; i < 4; ++i) {
			rends [1] = planes [0].GetComponent<Renderer> ();
			rends [2] = planes [1].GetComponent<Renderer> ();
			rends [3] = planes [2].GetComponent<Renderer> ();
		}

		//	Set parameters
		frameCount = 2;
		//string [] fileNames = {"combA01lena.png", "combB01lena.png"};
		string [] fileNames = {"02okno-light01.png","02okno-dark01.png"};
		texOrig = new Texture2D[frameCount];
		for (int frame = 0; frame < frameCount; ++frame) {
			texOrig[frame] = new Texture2D(2,2);
			texOrig[frame].LoadImage (File.ReadAllBytes(Application.dataPath + "/Images/" + fileNames[frame]));
		}
		//texOrig = (Texture2D)rends [0].material.GetTexture ("_MainTex");
		width = texOrig[0].width;
		height = texOrig[0].height;
		groupSize = 8;
		MIPlevels = 2;
		contrastCoefficient = 1.0f;
		exposednessCoefficient = 0.0f;
		//exposednessSigma = 0.2f;
		RT01 = ReadyTexture ();
		RT02 = ReadyTexture ();
		RT03 = ReadyTexture ();
		RT04 = ReadyTexture ();

		// Gauss 	= MIPlevels + 1	}
		// Laplace 	= MIPlevels + 1 }	* frameCount
		// Weights	= MipLevels + 1 }
		// Result	= MipLevels + 1 }
		// 7*MIP for 2 input frames

		//	Determine size of a single pyramid and whole computeBuffer
		MIPpixelCount = 0;
		for (int MIPlevel = 0; MIPlevel <= MIPlevels; ++MIPlevel) {
			int p = (int)Mathf.Pow (2, MIPlevel);
			MIPpixelCount += (width/p)*(height/p);
		}
		int size = frameCount*3*MIPpixelCount+MIPpixelCount;
		//	Allocate computeBuffer
		computeBuffer = new ComputeBuffer(size, 3*sizeof(float), ComputeBufferType.GPUMemory);

		//	Compute Gaussian pyramid
		kernelHandle = GaussCS.FindKernel ("GaussCS");
		GaussCS.SetInt ("width", width);
		GaussCS.SetInt ("height", height);
		GaussCS.SetInt ("mipLevels", MIPlevels);
		GaussCS.SetInt ("mipSize", MIPpixelCount);
		GaussCS.SetInt ("frameCount", frameCount);
		GaussCS.SetBuffer (kernelHandle, "data", computeBuffer);
		//	!!!
		//	TODO: Dispatch with one more group in each dimension to include non-divisible image dimensions
		//	!!!
		for (phase = 0; phase <= MIPlevels; ++phase) {
			for (int frame = 0; frame < frameCount; ++frame) {
				GaussCS.SetInt ("phase", phase);
				GaussCS.SetInt ("frame", frame);
				GaussCS.SetTexture (kernelHandle, "orig", texOrig[frame]);
				GaussCS.Dispatch (kernelHandle, width/Mathf.RoundToInt(groupSize*Mathf.Pow(2,phase)),
					height/Mathf.RoundToInt(groupSize*Mathf.Pow(2,phase)), 1);
			}
		}

		//	Compute Laplacian pyramid
		kernelHandle = LaplaceCS.FindKernel ("LaplaceCS");
		LaplaceCS.SetInt ("width", width);
		LaplaceCS.SetInt ("height", height);
		LaplaceCS.SetInt ("mipLevels", MIPlevels);
		LaplaceCS.SetInt ("mipSize", MIPpixelCount);
		LaplaceCS.SetInt ("frameCount", frameCount);
		LaplaceCS.SetBuffer (kernelHandle, "data", computeBuffer);
		//	!!!
		//	TODO: Dispatch with one more group in each dimension to include non-divisible image dimensions
		//	!!!
		for (int frame = 0; frame < frameCount; ++frame) {
			LaplaceCS.SetInt ("frame", frame);
			LaplaceCS.SetTexture (kernelHandle, "orig", texOrig[frame]);
			LaplaceCS.Dispatch (kernelHandle, width/groupSize, height/groupSize, 1);
		}

		//	Compute weights of pixels in input frame
		kernelHandle = WeightsCS.FindKernel ("WeightsCS");
		WeightsCS.SetInt ("width", width);
		WeightsCS.SetInt ("height", height);
		WeightsCS.SetInt ("mipLevels", MIPlevels);
		WeightsCS.SetInt ("mipSize", MIPpixelCount);
		WeightsCS.SetInt ("frameCount", frameCount);
		WeightsCS.SetFloat ("contrastCoef", 0f);//contrastCoefficient	exposednessCoefficient
		WeightsCS.SetFloat ("exposeCoef", 1f);
		//WeightsCS.SetFloat ("sigma", exposednessSigma);
		WeightsCS.SetBuffer (kernelHandle, "data", computeBuffer);
		for (int frame = 0; frame < frameCount; ++frame) {
			WeightsCS.SetInt ("frame", frame);
			WeightsCS.SetTexture (kernelHandle, "orig", texOrig[frame]);
			if (frame == 0)
				WeightsCS.SetTexture (kernelHandle, "weights", RT01);
			if (frame == 1)
				WeightsCS.SetTexture (kernelHandle, "weights", RT02);
			WeightsCS.Dispatch (kernelHandle, width/groupSize, height/groupSize, 1);
		}




		//	Combine weighted laplacian pyramids
		kernelHandle = CombineCS.FindKernel ("CombineCS");
		CombineCS.SetInt ("width", width);
		CombineCS.SetInt ("height", height);
		CombineCS.SetInt ("mipLevels", MIPlevels);
		CombineCS.SetInt ("mipSize", MIPpixelCount);
		CombineCS.SetInt ("frameCount", frameCount);
		CombineCS.SetBuffer (kernelHandle, "data", computeBuffer);
		CombineCS.SetTexture (kernelHandle, "result", RT03);
		//	!!!
		//	TODO: Dispatch with one more group in each dimension to include non-divisible image dimensions
		//	!!!
		CombineCS.Dispatch (kernelHandle, width/groupSize, height/groupSize, 1);

		//	Collapse resulting pyramid
		kernelHandle = CollapseCS.FindKernel ("CollapseCS");
		CollapseCS.SetInt ("width", width);
		CollapseCS.SetInt ("height", height);
		CollapseCS.SetInt ("mipLevels", MIPlevels);
		CollapseCS.SetInt ("mipSize", MIPpixelCount);
		CollapseCS.SetInt ("frameCount", frameCount);
		CollapseCS.SetBuffer (kernelHandle, "data", computeBuffer);
		CollapseCS.SetTexture (kernelHandle, "result", RT04);
		//	!!!
		//	TODO: Dispatch with one more group in each dimension to include non-divisible image dimensions
		//	!!!
		for (phase = MIPlevels; phase >= 0; --phase) {
				CollapseCS.SetInt ("phase", phase);
				CollapseCS.Dispatch (kernelHandle, width / Mathf.RoundToInt (groupSize * Mathf.Pow (2, phase)),
					height / Mathf.RoundToInt (groupSize * Mathf.Pow (2, phase)), 1);
		}


		//CollapseCS.Dispatch (kernelHandle, width/groupSize, height/groupSize, 1);
		/*
		//	helper shader for investigation of contents of computeBuffer
		kernelHandle = CoordCS.FindKernel ("CoordCS");
		CoordCS.SetInt ("width", width);
		CoordCS.SetInt ("height", height);
		CoordCS.SetInt ("frameCount", frameCount);
		CoordCS.SetInt ("mipLevels", MIPlevels);
		CoordCS.SetInt ("mipSize", MIPpixelCount);
		CoordCS.SetBuffer (kernelHandle, "data", computeBuffer);
		CoordCS.SetTexture (kernelHandle, "orig", texOrig);
		CoordCS.SetTexture (kernelHandle, "result", coordTex);

		CoordCS.Dispatch (kernelHandle, width/groupSize, height/groupSize, 1);
		*/

		//	Display results
		rends [0].material.SetTexture ("_MainTex", RT01);
		rends [1].material.SetTexture ("_MainTex", RT02);
		rends [2].material.SetTexture ("_MainTex", RT03);
		rends [3].material.SetTexture ("_MainTex", RT04);

		/*
		//	Save results fo files
		SaveTextureToFile (RTlaplace, "Laplace");
		SaveTextureToFile (RTgauss, "Gauss");
		*/

		//	Release computeBuffer
		computeBuffer.Release ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SaveTextureToFile( RenderTexture texture, string fileName)
	{
		RenderTexture.active = texture;
		Texture2D tex2D = new Texture2D(texture.width, texture.height);
		tex2D.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
		tex2D.Apply();
		byte[] bytes = tex2D.EncodeToPNG();
		File.WriteAllBytes(Application.dataPath + "/../"+fileName+".png", bytes);
	}

	RenderTexture ReadyTexture() {
		RenderTexture tex = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB);
		tex.enableRandomWrite = true;
		tex.Create ();
		return tex;
	}
}

//float3 color = float3(0,0,0);
//float colors [10];
//float weights [10] ;
//float weightSum = 0;
//for (uint f = 0; f < frameCount; ++f) {
//	colors[f] = data.Load(Coord(id.xy,phase, WEIGHTS,f)).color.x;
//	weights[f] = data.Load(Coord(id.xy,phase, WEIGHTS,f)).color.x;
//	weightSum += weights[f];
//}
//for (uint f = 0; f < frameCount; ++f) {
//	color += color[fd]*(weights[f]/weightSum);
//}