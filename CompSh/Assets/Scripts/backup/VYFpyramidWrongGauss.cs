﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VYFpyramidWrongGauss : MonoBehaviour {

	public ComputeShader compSh;
	public GameObject[] planes;

	int width;
	int height;
	int gaussKernelHandle;
	int laplaceKernelHandle;
	RenderTexture laplace0;
	RenderTexture laplaceMip;
	RenderTexture gauss0;
	RenderTexture gaussMip;
	Texture2D texOrig;
	Renderer [] rends;

	// Use this for initialization
	void Start () {
		rends = new Renderer[4];
		rends [0] = GetComponent<Renderer> ();
		for (int i = 0; i < 4; ++i) {
			rends [1] = planes [0].GetComponent<Renderer> ();
			rends [2] = planes [1].GetComponent<Renderer> ();
			rends [3] = planes [2].GetComponent<Renderer> ();
		}

		texOrig = (Texture2D) rends[0].material.GetTexture ("_MainTex");

		width = texOrig.width;
		height = texOrig.height;

		laplace0 = ReadyTexture ();
		laplaceMip = ReadyTexture ();
		gauss0 = ReadyTexture ();
		gaussMip = ReadyTexture ();

		gaussKernelHandle = compSh.FindKernel ("CSGauss");
		compSh.SetInt ("width", width);
		compSh.SetInt ("height", height);
		compSh.SetTexture (gaussKernelHandle, "orig", texOrig);
		compSh.SetTexture (gaussKernelHandle, "gauss0", gauss0);
		compSh.SetTexture (gaussKernelHandle, "gaussMip", gaussMip);

		compSh.Dispatch (gaussKernelHandle, width/8+1, height/8+1, 1);

		rends [0].material.SetTexture ("_MainTex", gauss0);
		rends [1].material.SetTexture ("_MainTex", gaussMip);

		RenderTexture.active = gauss0;
		Texture2D gauss0R = new Texture2D(width, height);
		gauss0R.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		gauss0R.Apply();
		RenderTexture.active = gaussMip;
		Texture2D gaussMipR = new Texture2D(width, height);
		gaussMipR.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		gaussMipR.Apply();

		laplaceKernelHandle = compSh.FindKernel ("CSLaplace");
		compSh.SetInt ("width", width);
		compSh.SetInt ("height", height);
		compSh.SetTexture (laplaceKernelHandle, "orig", texOrig);
		compSh.SetTexture (laplaceKernelHandle, "gauss0R", gauss0R);
		compSh.SetTexture (laplaceKernelHandle, "gaussMipR", gaussMipR);
		compSh.SetTexture (laplaceKernelHandle, "laplace0", laplace0);
		compSh.SetTexture (laplaceKernelHandle, "laplaceMip", laplaceMip);

		compSh.Dispatch (laplaceKernelHandle, width/8+1, height/8+1, 1);

		rends [2].material.SetTexture ("_MainTex", laplace0);
		rends [3].material.SetTexture ("_MainTex", laplaceMip);

	}
	
	// Update is called once per frame
	void Update () {

		//rend.material.SetTexture("_MainTex",texShader);
		//rend.material.SetTextureScale("_MainTex",new Vector2 (64f/dim,64f/dim));
		//rend.material.mainTexture = tex;
	}

	RenderTexture ReadyTexture() {
		//RenderTexture tex = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
		RenderTexture tex = new RenderTexture(width, height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB);
		tex.enableRandomWrite = true;
		tex.Create ();
		return tex;
	}
}
