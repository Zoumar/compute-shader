﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dislpay : MonoBehaviour {

	public ComputeShader compSh;

	int kernelHandle;
	RenderTexture texShader;
	Texture2D texOrig;
	Renderer rend;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();

		texOrig = (Texture2D) rend.material.GetTexture ("_MainTex");

		int width = texOrig.width;
		int height = texOrig.height;

		texShader = new RenderTexture(width,height,24);
		texShader.enableRandomWrite = true;
		texShader.Create ();

		kernelHandle = compSh.FindKernel ("CSMain");
		compSh.SetTexture (kernelHandle, "result", texShader);
		compSh.SetTexture (kernelHandle, "orig", texOrig);
		compSh.SetInt ("width", width);
		compSh.SetInt ("height", height);

		compSh.Dispatch (kernelHandle, width/8, height/8, 1);




	}
	
	// Update is called once per frame
	void Update () {

		rend.material.SetTexture("_MainTex",texShader);
		//rend.material.SetTextureScale("_MainTex",new Vector2 (64f/dim,64f/dim));
//		rend.material.mainTexture = tex;
	}

	void RunComputeShader() {
	}
}
