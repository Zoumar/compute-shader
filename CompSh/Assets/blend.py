#
#   GAUSS KERNEL PARSE
#
s = "0.003765	0.015019	0.023792	0.015019	0.003765 0.015019	0.059912	0.094907	0.059912	0.015019 0.023792	0.094907	0.150342	0.094907	0.023792 0.015019	0.059912	0.094907	0.059912	0.015019 0.003765	0.015019	0.023792	0.015019	0.003765 "
a1 = []
for e in s.split():
    a1.append(float(e))
print(a1)
a2 = 0
for e in a1:
    a2+=e
print(a2)

s = ""
for i in range(len(a1)):
    s+=str(int(round(a1[i]*1000)))
    if (i+1)%5 == 0:
        s+=",\n"
    else:
        s+=", "
print()
print(s)

#3.6e-05, 0.000363, 0.001446, 0.002291, 0.001446, 0.000363, 3.6e-05, 0.000363, 0.003676, 0.014662, 0.023226, 0.014662, 0.003676, 0.000363, 0.001446, 0.014662, 0.058488, 0.092651, 0.058488, 0.014662, 0.001446, 0.002291, 0.023226, 0.092651, 0.146768, 0.092651, 0.023226, 0.002291, 0.001446, 0.014662, 0.058488, 0.092651, 0.058488, 0.014662, 0.001446, 0.000363, 0.003676, 0.014662, 0.023226, 0.014662, 0.003676, 0.000363, 3.6e-05, 0.000363, 0.001446, 0.002291, 0.001446, 0.000363, 3.6e-05


#
#   MIP SIZE COUNT
#
w = 1536
h = 2560
frameCount = 2
mipLevels = 9
mipPixelCount = 0

for mipLevel in range(mipLevels+1):
    p = 2**mipLevel
    mipPixelCount += (w/p)*(h/p);
print()
print("1 pyramid: "+str(mipPixelCount))
pixelCount = frameCount*(3*mipPixelCount+w*h) + mipPixelCount
print("pixel count: "+str(pixelCount))
print("bytes: "+str(pixelCount*3*4))



#
#   DOUBLE DIVISION WITH FLOATS
#
#count = 7;
#value = 0.0073812398871474;
count = 4
value = 15
r = (1.0 / count); # approximate reciprocal
r = r * (2.0 - count*r); # much better approximation
r = r * (2.0 - count*r); # should be full double precision by now.
result = value * r;

print()
print(result)
print(value/count)

 
#
#   EXPOSURE METRIC WEIGHT
#
import math
color = [0,0,0]
sigma = 0.2
w = (math.e**-(((color[0]-0.5)**2)/(2*sigma**2)))**3
print(w)