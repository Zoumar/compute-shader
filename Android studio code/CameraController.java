package com.example.miroslavnovotn.cameracontroller;


import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.opengl.GLES20;

import android.os.Handler;
import android.os.HandlerThread;


import android.util.Range;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;

import com.unity3d.player.UnityPlayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Created by Miroslav Novotný on 06.04.2017.
 */
public class CameraController extends HandlerThread  {


    MainActivity activity;
    CameraCharacteristics characteristics;
    Size PreviewSize;
    CameraDevice mCameraDevice;
    int[] texturePointer;
    CaptureRequest.Builder mPreviewBuilder;
    Handler backgroundHandler;
    CameraCaptureSession mPreviewSession;
    Handler mHandler;
    ArrayList<CaptureRequest> hdrRequests = new ArrayList<CaptureRequest>(2);

    TextureView textureView;
    SurfaceTexture texture;
    Surface surface;
    CameraCaptureSession.CaptureCallback CaptureCallback;
    ImageReader imageReader;

    private static final long MICRO_SECOND = 1000;
    private static final long MILLI_SECOND = MICRO_SECOND * 1000;
    private static final long ONE_SECOND = MILLI_SECOND * 1000;

    private long exposureTime = ONE_SECOND / 33;
    Range<Long> range = new Range<>(-2l,2l);;
    Object tagHigh = new Object();
    Object tagLow = new Object();


    boolean low = false;

    public CameraController(String _name) {
        super(_name);

        //initTexture();
        HandlerThread thread = new HandlerThread("cameraOpen");
        thread.start();



        mHandler = new Handler(thread.getLooper());
        hdrRequests.add(null);
        hdrRequests.add(null);

    }

    void initTexture()
    {

        texturePointer = new int[1];
        GLES20.glGenTextures ( 1, texturePointer, 0 );
        /*
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, texturePointer[0]);

        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);
        */

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texturePointer[0]);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

    }

    public void takePicture() {
            //this is taken over by burst repeating
    }

    public void setIso(int _iso) {
        mPreviewBuilder.set(CaptureRequest.SENSOR_SENSITIVITY, _iso);
        /*
        try {
            //mPreviewSession.abortCaptures();
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), CaptureCallback, backgroundHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        */
    }

    public void setExposure(int _exposure) {

        mPreviewBuilder.set(CaptureRequest.SENSOR_EXPOSURE_TIME, ((long) _exposure));
        /*

        try {
            //mPreviewSession.abortCaptures();
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), CaptureCallback, backgroundHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        */
    }

    public int getTexturePointer()
    {
        return texturePointer[0];
    }

    public void setMainActivity(MainActivity _main, TextureView _view) {
        activity = _main;
        textureView = _view;
    }

    public void startCamera(int _id)
    {
        if (_id != -1) {
            texturePointer = new int[1];
            texturePointer[0] = _id;
        }
        else
        {
            initTexture();
        }

        openCamera();
    }

    ImageReader.OnImageAvailableListener onImageAvailableListenerLow = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader){
                Image img = reader.acquireNextImage();
                if (img != null) {
                    Image.Plane[] _imgPlanes = img.getPlanes();
                    byte[] bytes;
                    activity.setPlaneCount(_imgPlanes.length);

                    byte[][] image = new byte[_imgPlanes.length][];

                    for (int i = 0; i < _imgPlanes.length; i++) {
                        java.nio.ByteBuffer buffer = img.getPlanes()[i].getBuffer();
                        bytes = new byte[buffer.remaining()];
                        buffer.get(bytes);
                        image[i] = bytes;
                    }
                    img.close();

                    if (activity.imageID == 0) {

                        low = true;
                        activity.nextImage();
                        activity.setPictureData(image, true);
                        UnityPlayer.UnitySendMessage("MobileCameraController", "setLow", "");
                        UnityPlayer.UnitySendMessage("MobileCameraController", "finishTextureLow", "");

                        activity.imageID = 1;
                    } else {
                        low = false;
                        activity.nextImage();
                        activity.setPictureData(image, false);
                        UnityPlayer.UnitySendMessage("MobileCameraController", "setHigh", "");
                        UnityPlayer.UnitySendMessage("MobileCameraController", "finishTextureHigh", "");
                        activity.imageID = 0;

                        try {
                            mPreviewSession.captureBurst(hdrRequests, CaptureCallback, backgroundHandler);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }

                    }
                }



        }
    };

    ImageReader.OnImageAvailableListener onImageAvailableListenerHigh = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader){
            Image img = reader.acquireNextImage();

            Image.Plane[] _imgPlanes = img.getPlanes();
            byte[] bytes;
            activity.setPlaneCount(_imgPlanes.length);

            byte[][] image = new byte[_imgPlanes.length][];

            for (int i = 0; i < _imgPlanes.length; i++) {
                java.nio.ByteBuffer buffer = img.getPlanes()[i].getBuffer();
                bytes = new byte[buffer.remaining()];
                buffer.get(bytes);
                image[i] = bytes;
            }

            low = false;

            activity.nextImage();
            activity.setPictureData(image,false);
            UnityPlayer.UnitySendMessage("MobileCameraController", "setHigh", "");
            UnityPlayer.UnitySendMessage("MobileCameraController", "finishTextureHigh", "");

            img.close();
        }
    };


    public void openCamera() {

        CaptureCallback = new CameraCaptureSession.CaptureCallback() {
            @Override
            public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request,
                                           TotalCaptureResult result) {
                if (request.getTag() == tagHigh)
                {

                }
                else
                {

                }
                //activity.nextImage();

            }
        };

        mHandler.post(new Runnable() {
            @Override
            public void run() {
            CameraManager manager = (CameraManager) activity.getSystemService(activity.getBaseContext().CAMERA_SERVICE);
            //imageReader = ImageReader.newInstance(640, 480, ImageFormat.JPEG, 1); //fps * 10 min
            //imageReader = ImageReader.newInstance(640, 480, ImageFormat.YUV_420_888, 1); //fps * 10 min

            imageReader = ImageReader.newInstance(640, 480, ImageFormat.YUV_420_888, 2); //fps * 10 min
            imageReader.setOnImageAvailableListener(onImageAvailableListenerLow, null);

            try
            {
                String cameraId = manager.getCameraIdList()[0];
                characteristics = manager.getCameraCharacteristics(cameraId);
                //range = characteristics.get(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE);
                if (range == null)
                {
                    range = new Range<>(-2l,2l);
                }
                //CameraCharacteristics.
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                PreviewSize = map.getOutputSizes(SurfaceTexture.class)[0];

                manager.openCamera(cameraId, mStateCallback, null);

            }

            catch(
            CameraAccessException e
            )
            {
                e.printStackTrace();
            }
        }
        });

    }



    public void updatePreview() {

        //mPreviewBuilder.set(CaptureRequest.CONTROL_MODE,CameraMetadata.CONTROL_MODE_OFF);


        HandlerThread thread = new HandlerThread("CameraPreview");
        thread.start();
        backgroundHandler = new Handler(thread.getLooper());


        /*
        try {
            //mPreviewSession.capture(mPreviewBuilder.build(), null, backgroundHandler);
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), CaptureCallback, backgroundHandler);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        */
    }


    public void startPreview() {

        //texture = new SurfaceTexture(texturePointer[0]);//myTexture.getSurfaceTexture();
        /*
        if (null == texture) {
            return;
        }
        */
        //texture.setDefaultBufferSize(PreviewSize.getWidth(),PreviewSize.getHeight());
        //texture.setDefaultBufferSize(640,480);

        //surface = new Surface(texture);

        /*
        if (textureView != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    surface = new Surface(texture);
                    textureView.setSurfaceTexture(texture);
                }
            });
        }
        */
        try {
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewBuilder.set(CaptureRequest.CONTROL_AE_MODE,CameraMetadata.CONTROL_AE_MODE_ON);
            mPreviewBuilder.set(CaptureRequest.COLOR_CORRECTION_MODE, CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE_OFF);


        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        mPreviewBuilder.addTarget(imageReader.getSurface());


        CameraCaptureSession.StateCallback _callback = new CameraCaptureSession.StateCallback() {

            @Override
            public void onConfigured(CameraCaptureSession session) {
                mPreviewSession = session;
                startBurst();
                //updatePreview();
            }

            @Override
            public void onConfigureFailed(CameraCaptureSession session) {

            }
        };

        Surface[] surfaceList = new Surface[1];
        surfaceList[0] = imageReader.getSurface();



        try {

            mCameraDevice.createCaptureSession(Arrays.asList(surfaceList), _callback, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    private void startBurst()
    {
        //mPreviewBuilder.set(CaptureRequest.SENSOR_SENSITIVITY, 100);
        //mPreviewBuilder.set(CaptureRequest.SENSOR_FRAME_DURATION, ONE_SECOND/30);

        //mPreviewBuilder.set(CaptureRequest.SENSOR_EXPOSURE_TIME, ONE_SECOND);
        mPreviewBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION,range.getLower().intValue());
        mPreviewBuilder.setTag(tagLow);
        hdrRequests.set(0, mPreviewBuilder.build());

        mPreviewBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION,range.getUpper().intValue());
        mPreviewBuilder.setTag(tagHigh);
        hdrRequests.set(1, mPreviewBuilder.build());

        try {
            mPreviewSession.captureBurst(hdrRequests, CaptureCallback, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }


    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice camera) {

            mCameraDevice = camera;
            startPreview();

        }

        @Override
        public void onDisconnected(CameraDevice camera) {

        }

        @Override
        public void onError(CameraDevice camera, int error) {

        }

    };
}