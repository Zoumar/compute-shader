package com.example.miroslavnovotn.cameracontroller;
import android.content.Context;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.view.TextureView;

import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {

    byte[][] pictureLow;
    byte[][] pictureHigh;

    public int getPlaneCount() {
        return planeCount;
    }

    public void setPlaneCount(int planeCount) {
        this.planeCount = planeCount;
    }

    int planeCount = 0;
    int pictureIndex = 0;
    int texturePointer = -1;
    public int imageID = 0;
    CameraController cameraController;
    TextureView textureView;

    @Override
    public void onCreate(Bundle _savedInstanceState) {
        super.onCreate(_savedInstanceState);

    }

    public CameraManager getCameraManager()
    {
        return (CameraManager) getSystemService(Context.CAMERA_SERVICE);
    }


    public void initCamera() {

        cameraController = new CameraController("camera");

        cameraController.setMainActivity(this, null);
        cameraController.startCamera(texturePointer);

    }

    public int getTexturePointer()
    {
        return cameraController.getTexturePointer();
    }

/*
    public int getWidth()
    {
        return CameraDataCenter.REAL_PHOTO_WIDTH;
    }

    public int getHeight()
    {
        return CameraDataCenter.REAL_PHOTO_HEIGHT;
    }
*/
    public void setPictureData(byte[][] _data, boolean _low)
    {
        if (_low)
        {
            pictureLow = _data;
        }
        else
        {
            pictureHigh = _data;
        }
    }


    public void nextImage()
    {
        pictureIndex++;
        UnityPlayer.UnitySendMessage("MobileCameraController","nextImage","");
    }


    public byte[] getPictureData(int _i, boolean _low)
    {
        if (_low)
        {
            return pictureLow[_i];
        }
        else
        {
            return pictureHigh[_i];
        }

    }

    public int getPictureIndex()
    {
        return pictureIndex;
    }

    public void setPictureIndex(int _newIndex)
    {
        pictureIndex = _newIndex;
    }
/*
    public void setWidth(int _width)
    {
        CameraDataCenter.REAL_PHOTO_WIDTH = _width;
    }

    public void setHeight(int _height)
    {
        CameraDataCenter.REAL_PHOTO_HEIGHT = _height;
    }
*/
    public void setIso(int _iso)
    {
        cameraController.setIso(_iso);
    }
    /*
    public void increaseExposure()
    {
        cameraController.increaseExposure();
    }

    public void decreaseExposure()
    {
        cameraController.decreaseExposure();
    }
    */



    public void setExposure(int _exposure)
    {
        cameraController.setExposure(_exposure);
    }

    /*
    public void saveToTexture()
    {
        cameraController.setSaveToTexture();
    }
    */

    public void setTexturePointer(int _texturePointer)
    {
        texturePointer = _texturePointer;
        //cameraController.setTexturePointer(_texturePointer);
    }
/*
    public void returnPictureBytes()
    {
        cameraController.returnImageAsBytes();
    }
    */


    public void TakePhoto()
    {
        nextImage();
        cameraController.takePicture();

    }

    public void interfaceMethod(CameraInterface callback) {
        String x = "aa";

    }


}



