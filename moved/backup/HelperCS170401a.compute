//////////////////////////////////////
//                                  //
//	GLOBAL VARIABLES                //
//                                  //
//////////////////////////////////////
#define GAUSS   0
#define LAPLACE 1
#define WEIGHTS 2
static uint pow2[13] = {1,2,4,8,16,32,64,128,256,512,1024,2048,4096};

//////////////////////////////////////
//                                  //
//	FUNCTIONS DECLARATIONS          //
//                                  //
//////////////////////////////////////
uint Coord(uint2 id,uint mipLevel, uint type, uint frame);
float3 SubsamplePixel(uint2 id, uint mipLevel, uint type, uint frame);

//////////////////////////////////////
//									//
//	HELPER FUNCTIONS				//
//									//
//////////////////////////////////////
/*	original
uint Coord(uint2 id,uint mipLevel, uint type, uint frame) {
// Description: Computes coordinates of pixel to ComputeBuffer
// mipLevel: 0 = original size, each next level has both its dimensions halved 
//    compared to its predecessor
// type:  0 = GAUSS   = Gaussian pyramid
//        1 = LAPLACE = Laplacian pyramid
//        2 = WEIGHTS = Weights pyramid
// frame: 0 = first frame
//        ...
//        frameCount = result 

	if (id.x >= width/pow2[mipLevel] || id.y >= height/pow2[mipLevel])
		return 0;
	uint c = frame*3*mipSize;	// skip data of previous frames
	if (frame != frameCount) {	// if we're not dealing with Result
		c += type*(mipSize);	// skip previous pyramids of this frame (we're not dealing with Result)
	} else {					// if we're dealing with the Result
		// Result has only Laplacian pyramid and collapsed pyramid (single full-sized image)
		// type 0: Laplacian Pyramid; type 1: collapsed pyramid
		if (type) {					// if we want collapsed pyramid of Result
			c += mipSize;			// then skip Laplacian pyramid
			c += width*id.y + id.x;	// and select wanted pixel
			return c;
		} else {					// if we want pixel from Result pyramid
			[unroll]
			for (uint i = 0; i < mipLevel; ++i){		// then skip previous levels of this pyramid
				c += (width/pow2[i])*(height/pow2[i]);
			}
			c += (width/pow2[mipLevel])*id.y + id.x;	// and select wanted pixel
			return c;
		}
	}	// we're NOT dealing with the Result
	[unroll]
	for (uint i = 0; i < mipLevel; ++i){		// skip previous levels of this pyramid
		c += (width/pow2[i])*(height/pow2[i]);
	}
	c += (width/pow2[mipLevel])*id.y + id.x;	// and select wanted pixel
	return c;        
}
*/

float3 SubsamplePixel(uint2 id, uint mipLevel, uint type, uint frame) {
	uint x2[] = {0,1,0,1};
	uint y2[] = {0,0,1,1};
	/*
	if (id.x % 2 != 0)
		id.x -=1;
	if (id.y % 2 != 0)
		id.y -=1;
	*/
	id *= 2;
	uint sum = 0;
	float3 color = float3(0,0,0);
	[unroll]
	for (int i = 0; i < 4 ; ++i) {
		uint x = id.x+x2[i];
		uint y = id.y+y2[i];
		if (x < 0 || x >= width/pow2[mipLevel] || y < 0 || y >= height/pow2[mipLevel]) {
			continue;
		}
		color += data.Load(Coord(uint2(x,y), mipLevel, type, frame)).color;
		sum += 1;
	}
	color /= sum;
	return color;
}

uint Coord(uint2 id,uint mipLevel, uint type, uint frame) {
// Description: Computes coordinates of pixel to ComputeBuffer
// mipLevel: 0 = original size, each next level has both its dimensions halved 
//    compared to its predecessor
// type:  0 = GAUSS   = Gaussian pyramid
//        1 = LAPLACE = Laplacian pyramid
//        2 = WEIGHTS = Weights pyramid
// frame: 0 = first frame
//        ...
//        frameCount = result 

	if (id.x >= width/pow2[mipLevel] || id.y >= height/pow2[mipLevel])
		return 0;
	uint c = 3*mipSize*frame;	// skip previous frames
	if (frame != frameCount) {	// if we're not dealing with Result
		c += type*(mipSize);	// skip previous pyramids of this frame (we're not dealing with Result)
	} else {					// if we're dealing with the Result
		// Result has only Laplacian pyramid and collapsed pyramid (single full-sized image)
		// type 0: Laplacian Pyramid; type 1: collapsed pyramid
		if (type) {					// if we want collapsed pyramid of Result
			c += mipSize;			// then skip Laplacian pyramid
			c += width*id.y + id.x;	// and select wanted pixel
			return c;
		} else {					// if we want pixel from Result pyramid
			[unroll]
			for (uint i = 0; i < mipLevel; ++i){		// then skip previous levels of this pyramid
				c += (width/pow2[i])*(height/pow2[i]);
			}
			c += (width/pow2[mipLevel])*id.y + id.x;	// and select wanted pixel
			return c;
		}
	}	// we're NOT dealing with the Result
	[unroll]
	for (uint i = 0; i < mipLevel; ++i){		// skip previous levels of this pyramid
		c += (width/pow2[i])*(height/pow2[i]);
	}
	c += (width/pow2[mipLevel])*id.y + id.x;	// and select wanted pixel
	return c;        
}

//		Available Barriers:
//	GroupMemoryBarrier();
//	GroupMemoryBarrierWithGroupSync();
//	DeviceMemoryBarrier();
//	DeviceMemoryBarrierWithGroupSync();
//	AllMemoryBarrier();
//	AllMemoryBarrierWithGroupSync();