﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSGauss
#pragma kernel CSLaplace

// Create a RenderTexture with enableRandomWrite flag and set it
// with cs.SetTexture

RWTexture2D<float4> laplace0;
RWTexture2D<float4> laplaceMip;
RWTexture2D<float4> gauss0;
RWTexture2D<float4> gaussMip;


Texture2D<float4> gauss0R;
Texture2D<float4> gaussMipR;
SamplerState samplerGauss0R;
SamplerState samplerGaussMipR;

Texture2D<float4> orig;
SamplerState samplerOrig;
int width;
int height;

/*
//////////////////////////////////////
//									//
//	VARIABLES DECLARATIONS			//
//									//
//////////////////////////////////////
//	FILTERS							//
//////////////////////////////////////

// moved to corresponding functions

//////////////////////////////////////
//	HELPER ARRAYS					//
//////////////////////////////////////
static float x3[9] = {-1,0,1,-1,0,1,-1,0,1};
static float y3[9] = {-1,-1,-1,0,0,0,1,1,1};
static float x5[25] = {-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2};
static float y5[25] = {-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2};
*/

//////////////////////////////////////
//									//
//	FUNCTIONS DECLARATIONS			//
//									//
//////////////////////////////////////
//float4 Filter3 (uint3 id, float3x3 mat, Texture2D source);
//float4 Filter5 (uint3 id, float mat[25],Texture2D source);
float4 Gauss3 (uint3 id, int mipLevel);
void Gauss5 (uint3 id, int mipLevel);
float4 Laplace3 (uint3 id, int mipLevel);
uint2 MipCoords(uint3 pos, uint mipLevel, bool gauss);
void Subsample(uint3 pos, uint mipLevel);
void Laplace(uint3 pos, uint mipLevel);

void Bar() {
	//GroupMemoryBarrier();
	GroupMemoryBarrierWithGroupSync();

	/*
	NO!
	DeviceMemoryBarrier();
	DeviceMemoryBarrierWithGroupSync();
	AllMemoryBarrier();
	AllMemoryBarrierWithGroupSync();
	*/
}

//////////////////////////////////////
//									//
//	MAIN KERNEL          			//
//									//
//////////////////////////////////////
[numthreads(8,8,1)]
void CSGauss (uint3 id : SV_DispatchThreadID)
{
	Gauss5(id, 0);
	for (uint mipLevel = 1; mipLevel < 10; ++mipLevel) {
		if (id.x < width/pow(2,mipLevel) && id.y < height/pow(2,mipLevel)) {
			Gauss5(id, mipLevel);
		}
	}
}

[numthreads(8,8,1)]
void CSLaplace (uint3 id : SV_DispatchThreadID)
{
	//laplace0[id.xy] = gauss0R.Load(id);
	//Laplace(id,0);
	for (uint mipLevel = 0; mipLevel < 10; ++mipLevel) {
		if (id.x < width/pow(2,mipLevel) && id.y < height/pow(2,mipLevel)) {
			//gaussMip[MipCoords(id, mipLevel, true)] = Gauss5(id, mipLevel);
			//Gauss5(id, mipLevel);
			Laplace(id,mipLevel);
		}
	}
}

//////////////////////////////////////
//									//
//	HELPER FUNCTIONS				//
//									//
//////////////////////////////////////

void Gauss5 (uint3 id, int mipLevel) {
	float matGauss5[25] = {	1,  4,  6,  4, 1,
							4, 16, 24, 16, 4,
							6, 24, 36, 24, 6,
							4, 16, 24, 16, 4,
							1,  4,  6,  4, 1};
	int x5[25] = {-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2};
	int y5[25] = {-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2};
	float4 color = float4(0,0,0,1);
	float4 tmp = float4(0,0,0,1);
	float sum = 0;
	for (int i = 0; i < 25; ++i) {
		int x = id.x*pow(2,mipLevel)+x5[i]*pow(2,mipLevel);
		int y = id.y*pow(2,mipLevel)+y5[i]*pow(2,mipLevel);
		if ((x < 0) || (x >= width) ||
			(y < 0) || (y >= height)) {
			continue;
		} 
		tmp = orig.Load(uint3(x,y,id.z));
		color += tmp * matGauss5[2+x5[i],2+y5[i]];
		sum += matGauss5[2+x5[i],2+y5[i]];
	}
	color = color/sum;
	color.w = 1;
	if (mipLevel == 0) {
		gauss0[id.xy] = color;
	} else {
		gaussMip[MipCoords(id, mipLevel, true)] = color;
	}
	if (id.x % 2 == 0 && id.y % 2 == 0) {
		gaussMip[MipCoords(id/2, mipLevel+1, false)] = color;
	}
	//gaussMip[MipCoords(id, mipLevel+1, true)] = color;
}

void Laplace(uint3 pos, uint mipLevel) {
	if (mipLevel == 0) {
		laplace0[pos.xy] = (orig.Load(pos) - gauss0R.Load(pos))*10;
	} else {
		laplaceMip[MipCoords(pos, mipLevel, false)] = (
			gaussMipR.Load(uint3(MipCoords(pos, mipLevel, false),0)) - 
			gaussMipR.Load(uint3(MipCoords(pos, mipLevel, true),0)) )*10;
	}
}

// computes coordinates to Gauss mip-texture
//   gauss determines whether to use upper or lower half of texture
uint2 MipCoords(uint3 pos, uint mipLevel, bool gauss) {
	uint x = 0;
	uint y = 0;
	if (gauss)
		y += height/2;
	for (uint i = 1; i < mipLevel; ++i) {
		x += uint(width /pow(2,i));
	}
	if (!gauss) 
		y += height/2 - height/pow(2,mipLevel);
	return uint2(x+pos.x,y+pos.y);
}


// create a subsampled version of blurred image
//	result will be at mipLevel
void Subsample(uint3 pos, uint mipLevel) {
	int x[] = {0,0,1,1};
	int y[] = {0,1,0,1};
	float4 color = float4 (0,0,0,1);
	int count = 0;

	if (mipLevel == 1) {
		for (int i = 0; i < 4; ++i) {
			if ((pos.x == 0 && x[i] == -1) ||
				(pos.x == width/2-1 && x[i] == 1) ||
				(pos.y == 0 && y[i] == -1) ||
				(pos.y == height/2-1 && y[i] == 1)) {
				continue;
			}
			uint2 offset = uint2(x[i],y[i]);
			//color += gauss0[pos.xy+offset];
			color += orig.Load(pos*2);
			//color += float4(0.25,0.25,0.25,0.25);
			// gauss0[uint2(pos.x+x[i], pos.y+y[i])];// pos.xy+uint2(x[i],y[i])];// float4(0.5,0.5,0.5,0.5);// orig.Load(uint3(2*pos.x+x[i],2*pos.y+y[i],pos.z)); //2*pos.x+x2[i], 2*pos.y+y2[i])
			count +=1;
		}
	
	} else {
		for (int i = 0; i < 4; ++i) {
			if ((pos.x == 0 && x[i] == -1) ||
				(pos.x == width/pow(2,mipLevel-1)-1 && x[i] == 1) ||
				(pos.y == 0 && y[i] == -1) ||
				(pos.y == height/pow(2,mipLevel-1)-1 && y[i] == 1)) {
				continue;
			}
			uint3 a = uint3(x[i],y[i],0);
			color += orig.Load(pos*pow(2,mipLevel));
			//color += orig.Load(2*pos+a);// gaussMip[MipCoords(2*pos+uint3(x[i],y[i],0), mipLevel-1, false)];
			count +=1;
		}
	}
	color = color/count;
	color.w = 1;
	gaussMip[MipCoords(pos,mipLevel,false)] = color;
}

float4 Gauss3 (uint3 id, int mipLevel) {
	float3x3 matGauss3 = {1, 2, 1,
					      2, 4, 2,
			 	 		  1, 2, 1};
	int x3[9] = {-1,0,1,-1,0,1,-1,0,1};
	int y3[9] = {-1,-1,-1,0,0,0,1,1,1};
	float4 color = float4(0,0,0,1);
	float4 tmp = float4(0,0,0,1);
	float sum = 0;
	for (int i = 0; i < 9; ++i) {
		if ((id.x == 0 && x3[i] == -1) ||
			(id.x == width-1 && x3[i] == 1) ||
			(id.y == 0 && y3[i] == -1) ||
			(id.y == height-1 && y3[i] == 1)) {
			continue;
		} 
		if (mipLevel == 0) {
			tmp = orig.Load(uint3(id.x+x3[i],id.y+y3[i],id.z));
		} else {
			//tmp = gauss0[uint2(id.x+x3[i],id.y+y3[i])];
			tmp = gauss0[uint2(id.x+x3[i],id.y+y3[i])];

		}
		color += tmp * matGauss3[1+x3[i]][1+y3[i]];
		sum += matGauss3[1+x3[i]][1+y3[i]];
	}
	color = color/sum;
	color.w = 1;
	return color;
}

float4 Laplace3 (uint3 id, int mipLevel) {
	float3x3 matLaplace3 = { 0,-1, 0,
						  	-1, 4,-1,
			 	 		   	 0,-1, 0};
	//float3x3 matLaplace3_2 = {-1,-1,-1, -1, 8,-1, -1,-1,-1};
	int x3[9] = {-1,0,1,-1,0,1,-1,0,1};
	int y3[9] = {-1,-1,-1,0,0,0,1,1,1};
	float4 color = float4(0,0,0,1);
	float4 tmp = float4(0,0,0,1);
	for (int i = 0; i < 9; ++i) {
		if ((id.x == 0 && x3[i] == -1) ||
			(id.x == width-1 && x3[i] == 1) ||
			(id.y == 0 && y3[i] == -1) ||
			(id.y == height-1 && y3[i] == 1)) {
			continue;
		} 
		if (mipLevel == 0) {
			tmp = orig.Load(uint3(id.x+x3[i],id.y+y3[i],id.z));
		} else {
			// !!!
			tmp = laplace0[uint2(id.x+x3[i],id.y+y3[i])];
		}
		color += tmp * matLaplace3[1+x3[i]][1+y3[i]];
	}
	color.w = 1;
	return color;
}
/*
float4 Filter3 (uint3 id, float3x3 mat, Texture2D source) {
	float3x3 matGauss3 = {1, 2, 1,
					      2, 4, 2,
			 	 		  1, 2, 1};
	float4 color = float4(0,0,0,1);
	float sum = 0;
	for (int i = 0; i < 9; ++i) {
		if ((id.x == 0 && x3[i] == -1) ||
			(id.x == width-1 && x3[i] == 1) ||
			(id.y == 0 && y3[i] == -1) ||
			(id.y == height-1 && y3[i] == 1)) {
			continue;
		} 
		float4 tmp = source.Load(uint3(id.x+x3[i],id.y+y3[i],id.z));
		color += tmp * mat[1+x3[i]][1+y3[i]];
		sum += mat[1+x3[i]][1+y3[i]];
	}
	//color = color/sum;
	color.w = 1;
	return color;
}

float4 Filter5 (uint3 id, float mat[25], Texture2D source) {
	float4 color = float4(0,0,0,1);
	float sum = 0;
	for (int i = 0; i < 25; ++i) {
		if ((id.x == 1 && x5[i] == -2) ||
			(id.x == 0 && x5[i] == -2) ||
			(id.x == 0 && x5[i] == -1) ||
			(id.x == width-1 && x5[i] == 1) ||
			(id.x == width-1 && x5[i] == 2) ||
			(id.x == width-2 && x5[i] == 2) ||
			(id.y == 1 && y5[i] == -2) ||
			(id.y == 0 && y5[i] == -2) ||
			(id.y == 0 && y5[i] == -1) ||
			(id.y == height-1 && y5[i] == 1) ||
			(id.y == height-1 && y5[i] == 2) ||
			(id.y == height-2 && y5[i] == 2)) {
			continue;
		} 
		float4 tmp = source.Load(uint3(id.x+x5[i],id.y+y5[i],id.z));
		color += tmp * mat[2+x5[i],2+y5[i]];
		sum += mat[2+x5[i],2+y5[i]];
	}
	color = color/sum;
	color.w = 1;
	return color;
}
*/