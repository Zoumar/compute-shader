﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain

// Create a RenderTexture with enableRandomWrite flag and set it
// with cs.SetTexture

RWTexture2D<float4> result;
Texture2D<float4> orig;
SamplerState samplerorig;
int width;
int height;

//////////////////////////////////////
//									//
//	FUNCTIONS DECLARATIONS			//
//									//
//////////////////////////////////////
float4 filter3(uint3 id, float3x3 mat);
float4 filter5(uint3 id, float mat[25]);


//////////////////////////////////////
//									//
//	VARIABLES DECLARATIONS			//
//									//
//////////////////////////////////////
//	FILTERS							//
//////////////////////////////////////
static float3x3 matLaplace = { 0,-1, 0,
							  -1, 4,-1,
				 	 		   0,-1, 0};
static float3x3 matLaplace2 = {-1,-1,-1,
							   -1, 8,-1,
				 	 		   -1,-1,-1};
static float3x3 matGauss3 = { 1, 2, 1,
						      2, 4, 2,
				 	 		  1, 2, 1};
static float matGauss5[25] = {1,  4,  6,  4, 1,
							  4, 16, 24, 16, 4,
							  6, 24, 36, 24, 6,
							  4, 16, 24, 16, 4,
							  1,  4,  6,  4, 1};
//////////////////////////////////////
//	HELPER ARRAYS					//
//////////////////////////////////////
static float x3[9] = {-1,0,1,-1,0,1,-1,0,1};
static float y3[9] = {-1,-1,-1,0,0,0,1,1,1};
static float x5[25] = {-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2,-2,-1,0,1,2};
static float y5[25] = {-2,-2,-2,-2,-2,-1,-1,-1,-1,-1,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2};

//////////////////////////////////////
//									//
//	MAIN KERNEL          			//
//									//
//////////////////////////////////////
[numthreads(8,8,1)]
void CSMain (uint3 id : SV_DispatchThreadID)
{
	//result[id.xy] = filter3(id, matGauss3); // uncomment sum division in filter3
	//result[id.xy] = filter5(id, matGauss5); // uncomment sum division in filter3
	result[id.xy] = filter3(id, matLaplace); // comment sum division in filter3
	//result[id.xy] = filter3(id, matLaplace2);	// comment sum division in filter3

	/*
	if (id.x % 16 == 0 | id.y % 16 == 0) {
		result[id.xy] = float4(orig.Load(id).x,orig.Load(id).y,orig.Load(id).z,1);
	} else {
		//result[id.xy] = float4(orig.Load(id).x*id.x/225.,orig.Load(id).y*id.y/225.,orig.Load(id).z,1);//orig.Load(id).x;
		result[id.xy] = orig.Load(id);
	}*/




	//result[id.xy] = orig.SampleLevel(samplerorig, coords, 0); // needs normalized coords
	//result[id.xy] = float4(id.x/255.,0,0,1);
	//result[id.xy] = float4(id.x & id.y, (id.x & 15)/15.0, (id.y & 15)/15.0, 0.0);
}

//////////////////////////////////////
//									//
//	HELPER FUNCTIONS				//
//									//
//////////////////////////////////////
float4 filter3 (uint3 id, float3x3 mat) {
	float4 color = float4(0,0,0,1);
	float sum = 0;
	for (int i = 0; i < 9; ++i) {
		if ((id.x == 0 & x3[i] == -1) |
			(id.x == width-1 & x3[i] == 1) |
			(id.y == 0 & y3[i] == -1) |
			(id.y == height-1 & y3[i] == 1)) {
			continue;
		} 
		float4 tmp = orig.Load(uint3(id.x+x3[i],id.y+y3[i],id.z));
		color += tmp * mat[1+x3[i]][1+y3[i]];
		sum += mat[1+x3[i]][1+y3[i]];
	}
	//color = color/sum;
	color.w = 1;
	return color;
}

float4 filter5 (uint3 id, float mat[25]) {
	float4 color = float4(0,0,0,1);
	float sum = 0;
	for (int i = 0; i < 25; ++i) {
		if ((id.x == 1 & x5[i] == -2) |
			(id.x == 0 & x5[i] == -2) |
			(id.x == 0 & x5[i] == -1) |
			(id.x == width-1 & x5[i] == 1) |
			(id.x == width-1 & x5[i] == 2) |
			(id.x == width-2 & x5[i] == 2) |
			(id.y == 1 & y5[i] == -2) |
			(id.y == 0 & y5[i] == -2) |
			(id.y == 0 & y5[i] == -1) |
			(id.y == height-1 & y5[i] == 1) |
			(id.y == height-1 & y5[i] == 2) |
			(id.y == height-2 & y5[i] == 2)) {
			continue;
		} 
		float4 tmp = orig.Load(uint3(id.x+x5[i],id.y+y5[i],id.z));
		color += tmp * mat[2+x5[i],2+y5[i]];
		sum += mat[2+x5[i],2+y5[i]];
	}
	color = color/sum;
	color.w = 1;
	return color;
}
